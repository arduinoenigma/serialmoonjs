@ECHO OFF

:again

tasklist | find "chrome.exe" > nul
if %errorlevel% == 0 goto killbrowser

:detect
 
set browserpath=%PROGRAMFILES%\Google\Chrome\Application\chrome.exe
if exist "%browserpath%" goto run

set browserpath=%PROGRAMFILES(x86)%\Google\Chrome\Application\chrome.exe
if exist "%browserpath%" goto run

cls
echo.
echo   Cannot find CHROME.EXE, ensure it is installed for all users
echo.
echo   This program will now quit...
echo.
pause
 
exit
 
:run

cls
echo.
echo   Welcome to the MoonJS AGC modified for an external Serial DSKY .
echo.
echo   A new instance of Chrome will be launched.
echo   This instance will be launched with a couple of parameters to allow executing the AGC code and 
echo   accessing the external DSKY through the serial port.
echo.
echo   ^>^>^> Please DO NOT use this instance of Chrome to browse the web, IT IS NOT COMPLETELY SAFE ^<^<^<
echo.
echo   ^>^>^> WHEN FINISHED WITH THE AGC ^<^<^<
echo   ^>^>^> PLEASE CLOSE THIS CHROME WINDOW ^<^<^<
echo   ^>^>^> before using Chrome to browse the web ^<^<^<
echo.

pause
 
start "" "%browserpath%" --allow-file-access-from-files --enable-experimental-web-platform-features "file:///%cd%/agc.html"

exit
 
:killbrowser

cls
echo.
echo   Chrome browser is already running.
echo.
echo   ATTEMPTING TO KILL PROCESS. SWITCH TO CHROME AND SAVE ANY WORK!!!
echo.
echo   PRESS CTRL+C TO CANCEL!!! or enter to continue
echo.
pause

cls
echo.
echo   WARNING!!! CHROME BROWSER WILL BE FORCIBLY KILLED
echo.
echo   SWITCH TO CHROME AND SAVE ANY WORK!!!
echo.
echo   PRESS CTRL+C TO CANCEL!!! or enter to continue
echo.
pause

cls
echo.
echo   LAST CHANCE
echo.
echo   CHROME BROWSER WILL BE FORCIBLY KILLED
echo.
echo   SWITCH TO CHROME AND SAVE ANY WORK!!!
echo.
echo   PRESS CTRL+C TO CANCEL!!!
echo   PRESS ENTER TO CONTINUE TO KILL CHROME, UNSAVED WORK WILL BE LOST.
echo.
pause
echo.

taskkill /IM "chrome.exe" /F >nul

tasklist | find "chrome.exe" > nul
if %errorlevel% == 0 goto exitbrowsermsg

cls

goto detect

:exitbrowsermsg
 
cls
echo.
echo   Chrome is already running and could not be forcibly killed.
echo.
echo   If this continuously fails:
echo   Try using the Task Manager to terminate any Chrome tasks.
echo.
echo   Please exit all instances of Chrome before launching this.
echo.
echo   This program will now try again, to cancel press Ctrl+C...
echo.
pause

goto again
 
exit
