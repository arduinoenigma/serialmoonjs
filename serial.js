/*
 * @license
 * Getting Started with Web Serial Codelab (https://todo)
 * Copyright 2019 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
 
 /*
 Turn on all indicators:
 ABCDEFGHIJKLMOQ  P88  V88  N88  X+88888 Y+88888 Z+88888 
 Turn off all indicators:
 abcdefghijklmoq  P    V    N    X       Y       Z       
 */
 "use strict";

 let port;
 let reader;
 let inputDone;
 let outputDone;
 let inputStream;
 let outputStream;
 
 let portready;
 
 const butConnect = document.getElementById("butConnect");
 
 document.addEventListener("DOMContentLoaded", () => {
   butConnect.addEventListener("click", clickConnect);
 
   console.log('checking Web Serial API is available...');
   
   // CODELAB: Add feature detection here.
   const notSupported = document.getElementById("notSupported");
   notSupported.classList.toggle("hidden", "serial" in navigator);
 });
 
 
 navigator.serial.addEventListener("disconnect", (e) => {
   // Remove `e.target` from the list of available ports.
  
   console.log("dsky conn lost");  
   toggleUIConnected(false);
   //disconnect(); //does not reach disconnect5 spot
   port = null;
   console.log("dsky conn lost done");  
   
 });
 
 /**
  * @name connect
  * Opens a Web Serial connection to a micro:bit and sets up the input and
  * output stream.
  */
 async function connect() {
   
   console.log('opening Serial port');
   
   // CODELAB: Add code to request & open port here.
   // - Request a port and open a connection.
   port = await navigator.serial.requestPort();
   
   var portparams;
   
    portparams = {
     baudRate: 9600,
     dataBits: 8,
     parity: "none",
     stopBits: 1,
     flowControl: "none",
     baudrate: 9600,
     databits: 8,
     stopbits: 1,
     rtscts: false
   }; 
   
   //console.log(portparams);
   await port.open(portparams);  
 
   // wait for a little before creating the streams, needed for Arduino Uno/Mega
   setTimeout(function() {
     
     console.log('creating Serial port Streams');
     
     // CODELAB: Add code setup the output stream here.
     const encoder = new TextEncoderStream();
     outputDone = encoder.readable.pipeTo(port.writable);
     outputStream = encoder.writable;
 
     // CODELAB: Add code to read the stream here.
     let decoder = new TextDecoderStream();
     inputDone = port.readable.pipeTo(decoder.writable);
     inputStream = decoder.readable;
 
     reader = inputStream.getReader();
     readLoop();
     
     dsky.forceupdate(4); 
   }, 900);                 // increase 900 in case it fails to communicate  
 }
 
 /**
  * @name disconnect
  * Closes the Web Serial connection.
  */
 async function disconnect() {
 
   console.log('disconnect1');
   console.log(reader);
   // CODELAB: Close the input stream (reader).
   if (reader) {
     await reader.cancel();
     await inputDone.catch(() => {});
     reader = null;
     inputDone = null;
   }
 
   console.log('disconnect2');
   // CODELAB: Close the output stream.
   if (outputStream) {
     await outputStream.getWriter().close();
     await outputDone;
     outputStream = null;
     outputDone = null;
   }
   
   console.log('disconnect3');
   // CODELAB: Close the port.
   if ((port) && (port.connected)) {
   await port.close();
   }
   
   console.log('disconnect4');
   port = null;
   
   console.log('disconnect5');
 }
 
 /**
  * @name clickConnect
  * Click handler for the connect/disconnect button.
  */
 async function clickConnect() {
   // CODELAB: Add disconnect code here.
 
    //console.log(butConnect.textContent);
    //console.log(port);
 
   if (port) {
     
   if (port.connected==false)
   {
     console.log("Force1");	
     toggleUIConnected(false);
     port = null;
     await disconnect();		
     console.log("Force2");	//disconnect throws an exception and we never get here
     return;
   }
   else
   {
     console.log("Here!");
     await disconnect();
     toggleUIConnected(false);
     return;
   }
   }
 
   // CODELAB: Add connect code here.
   await connect();
 
   toggleUIConnected(true);
 }
 
 /**
  * @name readLoop
  * Reads data from the input stream and displays it on screen.
  */
 async function readLoop() {
   //CODELAB: Add read loop here.
   while (true) {
     const { value, done } = await reader.read();
     if (value) {
       //console.log("char:",value); //BUG: disable
     //console.log("length:",value.length); 
     dsky.receiveexternalkey(value);
     }
     if (done) {
       console.log("[readLoop] DONE", done);
       reader.releaseLock();
       break;
     }
   }
 }
 
 function sendDSKYValues(value)
 {
     if (outputStream)
   {
     //console.log('value:['+value+']'); //BUG: disable
     writeToStream(value);			
   }
 }
 
 function sendDSKYIndicator(item, itmcolor) {
   
   var cmd=' ';
   
   if ( typeof sendDSKYIndicator.indicators == 'undefined' ) {
         sendDSKYIndicator.indicators = [];
     }
   
   if ( typeof sendDSKYIndicator.counter == 'undefined' ) {
         sendDSKYIndicator.counter = 0;
     }	
   
   //console.log(item); //BUG: disable
   //console.log(itmcolor);
   
   // if itmcolor is grey, send lowercase command (turn off indicator)
   if ((itmcolor==='rgb(136, 136, 136)') || (itmcolor=='undefined'))
   {
     cmd = item.toLowerCase();
   }
   else
   {		
     cmd = item.toUpperCase();
   }
   
   //console.log(sendDSKYIndicator.indicators);	
   //console.log(sendDSKYIndicator.indicators['p'+item.toUpperCase()]);	
   
   // only send indicator if it changed from last time
   if ((outputStream) && (sendDSKYIndicator.indicators['p'+item.toUpperCase()]!=cmd))
   {
     writeToStream(cmd);		
   }	
   
   //store status of indicator using pA,pB,pC,... as index
   sendDSKYIndicator.indicators['p'+item.toUpperCase()]= cmd;
   
   //every 100 packets, delete prior indicator status and refresh all.
   sendDSKYIndicator.counter++;
   if (sendDSKYIndicator.counter==100)
   {
     sendDSKYIndicator.indicators = [];
     sendDSKYIndicator.counter=0;
   }
 }
 
 /**
  * @name writeToStream
  * Gets a writer from the output stream and send the lines to the micro:bit.
  * @param  {...string} lines lines to send to the micro:bit
  */
 function writeToStream(...lines) {
   // CODELAB: Write to output stream
   const writer = outputStream.getWriter();
   lines.forEach(line => {
     //console.log("[SEND]", line);  //BUG: disable this
     writer.write(line + "\n");
   });
   writer.releaseLock();
 }
 
 /**
  * The code below is mostly UI code and is provided to simplify the codelab.
  */
 
 function toggleUIConnected(connected) {
   let lbl = "Connect";
   if (connected) {
     lbl = "Disconnect";
   }
   butConnect.textContent = lbl;
 }
 