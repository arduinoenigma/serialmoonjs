# SerialMoonJS

A version of https://github.com/siravan/moonjs modified to output the screen status and accept keystrokes from serial port to allow an Arduino based external DSKY to control the simulation.
The original simulator is available at: http://svtsim.com/moonjs/agc.html
This modified version must be self hosted and viewed by clicking the launch.cmd file
